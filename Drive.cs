﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RouteGenerator
{
    public class Drive
    {
        public Route RouteUsed { get; set; }
        public DateTime Date { get; set; }
        public float usedFuel { get; set; }

        override
        public string ToString()
        {
            return "( " + Date.ToString(Form1.dateTimeFormat) + "; " + RouteUsed.ToString() + "; " + usedFuel + " )";
        }

        public string Export()
        {
            return Date.ToString(Form1.dateTimeFormat) + ";" + RouteUsed.From + "->" + RouteUsed.To + "->" + RouteUsed.From + ";;" + RouteUsed.Reason + ";;" + Math.Round(RouteUsed.Distance) + ";" + String.Format("{0:0.0}" , usedFuel);
        }
    }
}
