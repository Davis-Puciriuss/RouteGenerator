﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RouteGenerator
{
    public partial class Form3 : Form
    {

        public Route route { get; set; } = null;

        public Form3()
        {
            InitializeComponent();
        }

        private void textBox5_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(textBox4.Text.Length <= 0)
            {
                textBox4.Text = "0";
            }
            if (textBox5.Text.Length <= 0)
            {
                textBox5.Text = "0";
            }

            route = new Route()
            {
                From = textBox1.Text,
                To = textBox2.Text,
                Reason = textBox3.Text,
                Deviance = Int32.Parse(textBox4.Text),
                Distance = float.Parse(textBox5.Text, CultureInfo.InvariantCulture.NumberFormat)
            };
        }

        private void Form3_Shown(object sender, EventArgs e)
        {
            if(route == null)
            {
                textBox1.Clear();
                textBox2.Clear();
                textBox3.Clear();
                textBox4.Text = "0";
                textBox5.Text = "0";
            }
            else
            {
                textBox1.Text = route.From;
                textBox2.Text = route.To;
                textBox3.Text = route.Reason;
                textBox4.Text = route.Deviance + "";
                textBox5.Text = route.Distance + "";
            }
            
        }
    }
}
