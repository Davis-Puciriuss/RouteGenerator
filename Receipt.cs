﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RouteGenerator
{
    public class Receipt
    {
        public float FuelLiters { get; set; }
        public float Cost { get; set; }
        public string Odometer { get; set; }
        public string Nr { get; set; }
        public string Location { get; set; }
        public string FuelType { get; set; }
        public DateTime Date { get; set; }

        override
        public string ToString()
        {
            return "(" + Date.ToString(Form1.dateTimeFormat) + "; " + FuelLiters  + "; " + Cost + ")";
        }

        public string Export()
        {
            return Date.ToString(Form1.dateTimeFormat) + ";" + Location + ";;" + Nr + ";" + FuelLiters + ";" + (Cost/FuelLiters) + ";" + Cost;
        }
    }
}
