﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.Serialization.Json;

namespace RouteGenerator
{
    public partial class Form1 : Form
    {

        private static string routePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), @"RouteGenerator\routes.json");
        private static string settingsPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), @"RouteGenerator\settings.txt");
        public static string dateTimeFormat = "yyyy-MM-dd";
        private float fuelConsumption = 0;

        List<Route> routes = new List<Route>();
        List<Receipt> receipts = new List<Receipt>();

        public Form1()
        {
            InitializeComponent();

            textBox1.Text = "0";
            comboBox1.SelectedIndex = 0;

            if (File.Exists(settingsPath))
            {
                string[] lines = File.ReadAllLines(settingsPath);
                if(lines.Length > 0) { 
                    textBox1.Text = lines[0];
                }
                if (lines.Length > 1)
                {
                    comboBox1.SelectedIndex = Int32.Parse(lines[1]);
                }
            }

            if (File.Exists(routePath))
            {
                string json = File.ReadAllText(routePath);

                using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(json)))
                {
                    // Deserialization from JSON  
                    DataContractJsonSerializer deserializer = new DataContractJsonSerializer(typeof(List<Route>));
                    routes = (List<Route>)deserializer.ReadObject(ms);
                    foreach (Route route in routes)
                    {
                        listBox2.Items.Add(route.ToString());
                    }
                }

            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (Form2 form2 = new Form2())
            {
                if(form2.ShowDialog() == DialogResult.OK)
                {
                    form2.receipt.FuelType = comboBox1.SelectedItem.ToString();
                    receipts.Add(form2.receipt);
                    listBox1.Items.Add(form2.receipt.ToString());
                    form2.receipt = null;
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            using (Form3 form3 = new Form3())
            {
                if (form3.ShowDialog() == DialogResult.OK)
                {
                    routes.Add(form3.route);
                    listBox2.Items.Add(form3.route.ToString());
                    form3.route = null;
                }
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if(textBox1.Text.Length > 0)
            {
                fuelConsumption = float.Parse(textBox1.Text);
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            DataContractJsonSerializer js = new DataContractJsonSerializer(typeof(List<Route>));
            MemoryStream msObj = new MemoryStream();
            js.WriteObject(msObj, routes);
            msObj.Position = 0;
            StreamReader sr = new StreamReader(msObj);
            string json = sr.ReadToEnd();
            sr.Close();
            msObj.Close();

            if (!Directory.Exists(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), @"RouteGenerator\")))
            {
                Directory.CreateDirectory(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), @"RouteGenerator\"));
            }

            File.WriteAllText(routePath, json);
            File.WriteAllText(settingsPath, textBox1.Text + Environment.NewLine + comboBox1.SelectedIndex);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(listBox1.Items.Count > 0 && listBox1.SelectedIndex != -1)
            {
                int selected = listBox1.SelectedIndex;
                listBox1.Items.RemoveAt(selected);
                receipts.RemoveAt(selected);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (listBox2.Items.Count > 0 && listBox2.SelectedIndex != -1)
            {
                int selected = listBox2.SelectedIndex;
                listBox2.Items.RemoveAt(selected);
                routes.RemoveAt(selected);
            }
        }

        private void listBox2_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (listBox2.SelectedIndex != -1)
            {
                using (Form3 form3 = new Form3())
                {
                    int selected = listBox2.SelectedIndex;
                    form3.route = routes[selected];
                    routes.RemoveAt(selected);
                    listBox2.Items.RemoveAt(selected);
                    DialogResult dr = form3.ShowDialog();
                    if (dr == DialogResult.OK)
                    {
                        routes.Insert(selected, form3.route);
                        listBox2.Items.Insert(selected, form3.route.ToString());
                        form3.route = null;
                    }
                    else if (dr == DialogResult.Cancel)
                    {
                        routes.Insert(selected, form3.route);
                        listBox2.Items.Insert(selected, form3.route.ToString());
                        form3.route = null;
                    }
                }
            }
        }

        private void listBox1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if(listBox1.SelectedIndex != -1)
            {
                using (Form2 form2 = new Form2())
                {
                    int selected = listBox1.SelectedIndex;
                    form2.receipt = receipts[selected];
                    receipts.RemoveAt(selected);
                    listBox1.Items.RemoveAt(selected);
                    DialogResult dr = form2.ShowDialog();
                    if (dr == DialogResult.OK)
                    {
                        receipts.Insert(selected, form2.receipt);
                        listBox1.Items.Insert(selected, form2.receipt.ToString());
                        form2.receipt = null;
                    }
                    else if (dr == DialogResult.Cancel)
                    {
                        receipts.Insert(selected, form2.receipt);
                        listBox1.Items.Insert(selected, form2.receipt.ToString());
                        form2.receipt = null;
                    }
                }
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if(routes.Count <= 0)
            {
                MessageBox.Show("Nav pievienots neviens ceļš!", "Error !", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (receipts.Count <= 0)
            {
                MessageBox.Show("Nav pievienots neviens čeks ar degvielas uzpildi!", "Error !", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if(fuelConsumption <= 0)
            {
                MessageBox.Show("Nav ievadīts vidējais degvielas patēriņš!", "Error !", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            List<Route> sortedRoutes = routes.OrderByDescending(o => o.Distance).ToList();
            List<Receipt> sortedReceipts = receipts.OrderBy(o => o.Date).ToList();
            List<Drive> drives = new List<Drive>();

            float totalKm = 0;
            float coveredKm = 0;

            Random random = new Random((int) DateTime.Now.Ticks);
            bool sameDate = false;

            foreach(Receipt receipt in sortedReceipts)
            {
                totalKm += (receipt.FuelLiters / fuelConsumption) * 100;
                if (totalKm - coveredKm <= 0) continue;

                DateTime currentTime = receipt.Date;

                for (int i = 0; i < sortedRoutes.Count; i++)
                {
                    if (drives.Count >= 12) break;
                    Route route = sortedRoutes[i];
                    if (route.Distance <= (totalKm - coveredKm))
                    {
                        if (currentTime.DayOfWeek == DayOfWeek.Saturday)
                        {
                            currentTime = currentTime.AddDays(2);
                        }
                        else if (currentTime.DayOfWeek == DayOfWeek.Sunday)
                        {
                            currentTime = currentTime.AddDays(1);
                        }
                        coveredKm += route.Distance;
                        drives.Add(new Drive()
                        {
                            RouteUsed = new Route(route),
                            Date = currentTime,
                            usedFuel = route.Distance * fuelConsumption / 100
                        });

                        if(random.Next(101) <= 50 || sameDate)
                        {
                            currentTime = currentTime.AddDays(1);
                            sameDate = false;
                        }
                        else
                        {
                            sameDate = true;
                        }
                        
                        i--;
                    }
                }
            }

            for (int i = 0; i < drives.Count; i++)
            {
                if ((totalKm - coveredKm) > 0)
                {
                    float addKm = 0f;
                    if ((totalKm - coveredKm) / drives[i].RouteUsed.Distance * 100 <= drives[i].RouteUsed.Deviance)
                    {
                        addKm = drives[i].RouteUsed.Distance * (totalKm - coveredKm) / drives[i].RouteUsed.Distance;
                    }
                    else
                    {
                        addKm = drives[i].RouteUsed.Distance * random.Next(drives[i].RouteUsed.Deviance/2, drives[i].RouteUsed.Deviance) / 100;
                    }
                    drives[i].RouteUsed.Distance = drives[i].RouteUsed.Distance + addKm;
                    drives[i].usedFuel = drives[i].RouteUsed.Distance * fuelConsumption / 100;
                    coveredKm += addKm;
                }
                else
                {
                    break;
                }
            }

            if ((totalKm - coveredKm) > 0)
            {
                MessageBox.Show("Nav iespējams izbraukt visu iepildīto degvielu! Atlikuši " + (totalKm - coveredKm), "Warning !", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            string exportedData = "No: " + new DateTime(sortedReceipts.First().Date.Year, sortedReceipts.First().Date.Month, 1).ToString("dd-MM-yyyy") + Environment.NewLine
                    + "Līdz: " + new DateTime(sortedReceipts.First().Date.Year, sortedReceipts.First().Date.Month + 1, 1).AddDays(-1).ToString("dd-MM-yyyy") + Environment.NewLine
                    + "Odometrs Sākumā: " + sortedReceipts.First().Odometer + Environment.NewLine
                    + "Odometrs Beigās: " + Math.Round(Int32.Parse(sortedReceipts.Last().Odometer) + ((sortedReceipts.Last().FuelLiters / fuelConsumption) * 100)) + Environment.NewLine
                    + "Degviela: " + comboBox1.SelectedItem.ToString() + Environment.NewLine;

            exportedData += @"Nr.;Datums;Degvielas iegādes vietas adrese;;Čeka Nr.;Daudzums l;Cena;Summa EUR" + Environment.NewLine;

            for (int i = 0; i < sortedReceipts.Count; i++)
            {
                exportedData += i + ";" + sortedReceipts[i].Export() + Environment.NewLine;
            }

            exportedData += Environment.NewLine + @"Nr.;Datums;Maršruts;;Pamatojums;;Nobrauktie km;Patērētā degviela l" + Environment.NewLine;

            for (int i = 0; i < drives.Count; i++)
            {
                exportedData += i + ";" + drives[i].Export() + Environment.NewLine;
            }

            SaveFileDialog sfd = new SaveFileDialog();
            sfd.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            sfd.Title = "Save Text File";
            sfd.DefaultExt = "txt";
            sfd.Filter = "Text files (*.txt)|*.txt";
            sfd.FilterIndex = 1;
            sfd.RestoreDirectory = true;
            sfd.FileName = @"Atskaite_" + DateTime.Now.ToString("yyyy-MM-dd");

            DialogResult dr = sfd.ShowDialog();

            if(dr == DialogResult.OK)
            {
                File.WriteAllText(sfd.FileName, exportedData);
            }
        }
    }
}
