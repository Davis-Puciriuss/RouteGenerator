﻿using System;
using System.Runtime.Serialization;

namespace RouteGenerator
{
    [DataContract]
    public class Route
    { 
        [DataMember]
        public string From { get; set; }
        [DataMember]
        public string To { get; set; }
        [DataMember]
        public string Reason { get; set; }
        [DataMember]
        public int Deviance { get; set; }
        [DataMember]
        public float Distance { get; set; }

        public Route()
        {

        }

        public Route(Route route)
        {
            this.From = route.From;
            this.To = route.To;
            this.Reason = route.Reason;
            this.Deviance = route.Deviance;
            this.Distance = route.Distance;
        }

        override
        public string ToString()
        {
            return "(" + From + "->" + To + "->" + From + ";" + Distance + ")";
        }
    }
}
