﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RouteGenerator
{
    public partial class Form2 : Form
    {

        public Receipt receipt { get; set; }

        public Form2()
        {
            InitializeComponent();
        }
        
        private void Form2_Shown(object sender, EventArgs e)
        {
            if(receipt == null)
            {
                textBox1.Text = "0";
                textBox2.Text = "0";
                textBox3.Text = "0";
                textBox4.Text = "0";
                textBox5.Clear();
                dateTimePicker1.Value = DateTime.Now;
            }
            else
            {
                textBox1.Text = receipt.FuelLiters + "";
                textBox2.Text = receipt.Cost + "";
                textBox3.Text = receipt.Odometer;
                textBox4.Text = receipt.Nr;
                textBox5.Text = receipt.Location;
                dateTimePicker1.Value = receipt.Date;
            }
            
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            receipt = new Receipt()
            {
                FuelLiters = float.Parse(textBox1.Text, CultureInfo.InvariantCulture.NumberFormat),
                Cost = float.Parse(textBox2.Text, CultureInfo.InvariantCulture.NumberFormat),
                Odometer = textBox3.Text,
                Nr = textBox4.Text,
                Location = textBox5.Text,
                Date = dateTimePicker1.Value,
            };
        }

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }
    }
}
